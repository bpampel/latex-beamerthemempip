PROJNAME=beamerthemempip

PACKAGE_STY := $(wildcard *.sty)
TEST_DIR := example
TEST_SRC := $(TEST_DIR)/example_presentation.tex
TEST_PDF := $(TEST_DIR)/example_presentation.pdf
TEST_FIGURES := $(TEST_DIR)/example_presentation
DEST_DIR := $(shell kpsewhich -var-value=TEXMFHOME)
INSTALL_DIR := $(DEST_DIR)/tex/latex/beamerthemempip
CACHE_DIR   := $(shell pwd)/.latex-cache

COMPILE_TEX = latexmk -interaction=batchmode -silent -pdf -output-directory=$(CACHE_DIR) > /dev/null


.PHONY: install uninstall test figures clean


install:
	@install -D -t $(INSTALL_DIR)/ $(PACKAGE_STY)
	@echo "Installed to "$(INSTALL_DIR)


uninstall:
	@rm $(addprefix $(INSTALL_DIR)/, $(PACKAGE_STY))
	@rmdir $(INSTALL_DIR)


test: | clean
	@$(COMPILE_TEX) $(TEST_SRC)
	@cp $(CACHE_DIR)/$(notdir $(TEST_PDF)) $(TEST_PDF)


figures: | test
	@pdftoppm $(TEST_PDF) $(TEST_FIGURES) -png


clean:
	@rm -rf $(CACHE_DIR)

beamertheme for mpip
====================

This is an attempt to recreate the powerpoint template of the MPI-P for the use with LaTeX.
At the moment it provides roughly a similar look via tikz shapes.
As it is still work in progress, there might be major adjustments in the future.

*Note that this is not an official theme in any way.*

Title page with figure |  Title page without figure | Exemplary slide
:-----------------:|:-------------------------:|:-----------------:
![Title page](example/example_presentation-1.png)  | ![Title page without graphic](example/example_presentation-2.png) |   ![Example slide](example/example_presentation-3.png)

Installation
------------

(Texlive)
There are multiple possibilities to get the theme working:
* For a single project: copy all .sty files into the project folder.
* User install: use `make install` to copy the .sty files to the TEXMFHOME folder.
* Global install: (only manually at the moment) Move the .sty files to the subfolder `tex/latex/beamerthemempip` under TEXMFLOCAL (usually /usr/share/texmf/).

You will likely need to update your TeX database after installing, e.g. by running `texhash` on the folder.

Usage
-----

Just include
```
\usetheme{mpip}
```
in your preamble to use the theme.
As no mpip logo is provided directly (due to possible license issues) you need to specify the path to it with the command
```
\logopath{path_to_logo.pdf}
```
or the compilation will fail.
The theme was designed for the "Logo with Minerva" in green.

See also the tex file of the examplary presentation for further information.
You have to copy/link the Logo to `MPIP_Logo.pdf` or enter a correct logo path before compiling.
Then you can run `make test`, it tries to generate the exemplary pdf file. If should exit without error if the theme was installed correctly.

Other commands
-------

The footer, e.g. on the first page, can be turned off by using the `\footerfalse` command before the respective frame.
Don't forget to turn it on again (with `\footertrue`).
